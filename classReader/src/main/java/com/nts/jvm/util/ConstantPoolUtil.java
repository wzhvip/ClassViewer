package com.nts.jvm.util;


import com.nts.jvm.constantpool.*;

import java.util.HashMap;

public class ConstantPoolUtil {

    private HashMap<Integer, CP_info> poolMap;

    public ConstantPoolUtil(HashMap<Integer, CP_info> poolMap) {
        this.poolMap = poolMap;
    }

    public String formatCpInfo(CP_info info) {
        return formatCpInfo(info, true);
    }

    /**
     * @param info
     * @return
     */
    public String formatCpInfo(CP_info info, boolean debug) {
        int tag = info.getTag();

        switch (tag) {
            case 1: {
                CONSTANT_Utf8_info classInfo = (CONSTANT_Utf8_info) info;
                return  "Utf8  " + classInfo.getUtf8();

            }
            case 3: {
                CONSTANT_Integer_info classInfo = (CONSTANT_Integer_info) info;
                return "Integer ";
            }
            case 4: {
                CONSTANT_Float_info classInfo = (CONSTANT_Float_info) info;
                return "Float ";
            }

            case 5: {

                CONSTANT_Long_info classInfo = (CONSTANT_Long_info) info;
                return "Long";
            }
            case 6: {

                CONSTANT_Double_info classInfo = (CONSTANT_Double_info) info;
                return "Double ";
            }
            case 7: {
                CONSTANT_Class_info classInfo = (CONSTANT_Class_info) info;
                return (debug ? (
                        "Class #" + classInfo.getNameIndex()
                                + " // ") : "")

                        + getString(classInfo.getNameIndex());
            }

            case 8: {
                CONSTANT_String_info classInfo = (CONSTANT_String_info) info;

                return (debug ? ("String #" + classInfo.getStringIndex() + " // ") : "")

                        + getString(classInfo.getStringIndex());
            }

            case 9: {
                CONSTANT_Fieldref_info classInfo = (CONSTANT_Fieldref_info) info;
                return
                        (debug ? (
                                "Fieldref #" + classInfo.getClassIndex() + " // ") : ""
                        )

                                +
                                getString(classInfo.getClassIndex());
            }

            case 10: {
                CONSTANT_Methodref_info classInfo = (CONSTANT_Methodref_info) info;
                return (debug ? (

                        "Methodref #" + classInfo.getClassIndex() + ".#" +
                                classInfo.getNameAndTypeIndex()
                                + " // "

                ) : "")

                        + getString(classInfo.getClassIndex())
                        + " " + getString(classInfo.getNameAndTypeIndex())
                        ;
            }

            case 11: {
                CONSTANT_InterfaceMethodref_info classInfo = (CONSTANT_InterfaceMethodref_info) info;
                return
                        debug ? (
                                "InterfaceMethodref #" + classInfo.getClassIndex() + ".#" +
                                        classInfo.getNameAndTypeIndex()
                                        + " // "
                        ) : ""

                                + getString(classInfo.getClassIndex())
                                + " " + getString(classInfo.getNameAndTypeIndex())
                        ;
            }

            case 12: {
                CONSTANT_NameAndType_info classInfo = (CONSTANT_NameAndType_info) info;
                return
                        (debug ? (
                                "NameAndType #" + classInfo.getNameIndex() + ".#" +
                                        classInfo.getDescriptorIndex()
                                        + " // "
                        ) : ""
                        )
                                + getString(classInfo.getNameIndex())
                                + " " + getString(classInfo.getDescriptorIndex())
                        ;
            }

            case 15: {
                CONSTANT_MethodHandle_info classInfo = (CONSTANT_MethodHandle_info) info;
                return
                        (debug ? (
                                "MethodHandle #" + classInfo.getReferenceKind() + ".#" +
                                        classInfo.getReferenceIndex()
                                        + " // "

                        ) : ""
                        )
                                + getString(classInfo.getReferenceKind())
                                + " " + getString(classInfo.getReferenceIndex())
                        ;
            }

            case 16: {
                CONSTANT_MethodType_info classInfo = (CONSTANT_MethodType_info) info;
                return (debug ? (

                        "MethodType  #" + classInfo.getDescriptorIndex()
                                + " // "
                ) : ""
                )
                        + " " + getString(classInfo.getDescriptorIndex())
                        ;
            }


            case 18: {
                CONSTANT_InvokeDynamic_info classInfo = (CONSTANT_InvokeDynamic_info) info;
                return
                        (debug ? (
                                "InvokeDynamic #" + classInfo.getBootStrapMethodAttrIndex()
                                        + ".#" +
                                        classInfo.getNameAndTypeIndex()
                                        + " // "
                        ) : ""
                        )
                                + getString(classInfo.getBootStrapMethodAttrIndex())
                                + " " + getString(classInfo.getNameAndTypeIndex())
                        ;
            }


            default:
                return "";

        }

    }




    private String getString(int index) {
        return getString(index, 0);
    }

    /**
     * @param index
     * @return
     */

    private String getString(int index, int level) {
        level++;
        if (level >= 5) return "";
        CP_info cpInfo = poolMap.get(index);
        if (cpInfo instanceof CONSTANT_Utf8_info) {
            return ((CONSTANT_Utf8_info) cpInfo).getUtf8();
        } else {
            return formatCpInfo(cpInfo, false);

        }
    }


}
