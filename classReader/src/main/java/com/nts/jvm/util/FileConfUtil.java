package com.nts.jvm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class FileConfUtil {

	private static String confName = "class_conf.properties";

	public static String getConfPath() {

		// 获取程序当前路径
		String strDir = System.getProperty("user.dir");
		// 将路径分隔符更换
		String folderpath = strDir.replace('\\', File.separatorChar);
		String pathName = folderpath + File.separatorChar + confName;
		System.err.println(pathName);
		return pathName;
	}

	/**
	 * 
	 * @return
	 */
	public static String readConf(String key) {

		try {

			String pathName = getConfPath();
			Properties pro = new Properties();
			pro.load(new FileInputStream(new File(pathName)));
			return pro.getProperty(key);

		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
 * 
 */
	public static void saveConf(String key, String value) {
		try {

			Properties pro = new Properties();
			String pathName = getConfPath();
			File file = new File(pathName);
			FileOutputStream out = new FileOutputStream(file);
			pro.setProperty(key, value);
			pro.store(out, "classView");
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
