package com.nts.jvm.gui;

import java.awt.Dimension;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSplitPane;
import javax.swing.filechooser.FileFilter;

import com.nts.jvm.ClassFile;
import com.nts.jvm.ClassReader;
import com.nts.jvm.util.FileConfUtil;

public class ClassViewFrame extends JFrame implements DropTargetListener {

	/**
	 * ClassViewFrame
	 */
	private static final long serialVersionUID = 5034370401093423870L;
	private JSplitPane mainPanel;
	private MethodPanel methodPanel;
	private ClassInfoPanel classInfoPanel;

	public ClassViewFrame() {
		setTitle("Class Viewer");

		mainPanel = new JSplitPane();
		mainPanel.setDividerLocation(260);
		mainPanel.setLeftComponent(methodPanel = new MethodPanel());
		mainPanel.setRightComponent(classInfoPanel = new ClassInfoPanel());
		this.getContentPane().add(mainPanel);
		this.setSize(new Dimension(800, 600));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar mainBar = new JMenuBar();
		JMenu file = new JMenu("File");
		mainBar.add(file);
		JMenuItem openItem = new JMenuItem("Open...");
		file.add(openItem);
		openItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				String classPath = FileConfUtil.readConf("classPath");
				if (!chooser.equals("")) {
					File curFile = new File(classPath);

					chooser.setCurrentDirectory(curFile.getParentFile());
				}

				chooser.setFileFilter(new FileFilter() {
					@Override
					public boolean accept(File f) {
						return f.isDirectory() || f.getName().endsWith("class");
					}

					@Override
					public String getDescription() {
						return "(*.class)|Class File";
					}
				});

				int r = chooser.showOpenDialog(ClassViewFrame.this);
				if (JFileChooser.APPROVE_OPTION == r) {

					update(chooser.getSelectedFile().getAbsolutePath());

				}
			}
		});

		this.setDropTarget(new DropTarget(this, DnDConstants.ACTION_COPY_OR_MOVE, this, true));

		this.setJMenuBar(mainBar);

		this.setVisible(true);

		ClassReader reader = new ClassReader();

		try {
			String classPath = FileConfUtil.readConf("classPath");
			if (!classPath.equals("")) {
				ClassFile classFile = reader.readClassFile(classPath);
				methodPanel.update(classFile);
				classInfoPanel.update(classFile);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void update(String classPath) {
		ClassReader reader = new ClassReader();
		try {
			ClassFile classFile = reader.readClassFile(classPath);
			methodPanel.update(classFile);
			classInfoPanel.update(classFile);
			FileConfUtil.saveConf("classPath", classPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		new ClassViewFrame();

	}

	private List<File> fileList;

	@SuppressWarnings("unchecked")
	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		try {

			fileList = (List<File>) dtde.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {

	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {

	}

	@Override
	public void dragExit(DropTargetEvent dte) {

	}

	@Override
	public void drop(DropTargetDropEvent dtde) {
		if (fileList != null && fileList.size() > 0) {
			String filePath = fileList.get(0).getAbsolutePath();
			update(filePath);

		}
		System.err.println("exit" + dtde);
		dtde.dropComplete(true);
	}

}
