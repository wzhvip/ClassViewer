package com.nts.jvm.gui;

import com.nts.jvm.ClassFile;

import javax.swing.*;
import java.awt.*;
import java.awt.dnd.DropTarget;

public class ClassInfoPanel extends JPanel implements ClassListiner {

	/**
	 * ClassInfoPanel
	 */
	private static final long serialVersionUID = -4023952259692575216L;
	private JTextArea area = new JTextArea();

	public ClassInfoPanel() {
		this.setLayout(new BorderLayout());
		// area.setBorder(new LineNumberBorder());
		area.setAlignmentX(200);
		// area.setMargin(new Insets(0,200,0,10));
		JScrollPane mainPanel = new JScrollPane();
		mainPanel.getViewport().add(area, BorderLayout.CENTER);
		this.add(mainPanel, BorderLayout.CENTER);

	}

	public JTextArea getArea() {
		return area;
	}

	@Override
	public void update(ClassFile file) {
		area.setText(file.toString());
		area.setEditable(false);

	}
}
