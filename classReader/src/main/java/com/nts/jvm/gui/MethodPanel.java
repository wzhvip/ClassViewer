package com.nts.jvm.gui;

import com.nts.jvm.ClassFile;
import com.nts.jvm.methodinfo.MethodInfo;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Vector;

public class MethodPanel extends JPanel implements ClassListiner {

	/**
	 * MethodPanel
	 */
	private static final long serialVersionUID = 1765832792106182251L;
	private JList listBox = new JList();

	public MethodPanel() {

		JScrollPane mainPanel = new JScrollPane();

		this.setLayout(new BorderLayout());
		mainPanel.getViewport().add(listBox);
		this.add(mainPanel, BorderLayout.CENTER);

	}

	public JList getArea() {
		return listBox;
	}

	public void update(ClassFile file) {
		ArrayList<MethodInfo> methodList = file.getMethods();
		Vector<String> data = new Vector<String>();
		for (MethodInfo info : methodList) {
			data.add(info.getPrintName());
		}
		listBox.setListData(data);
	}
}
