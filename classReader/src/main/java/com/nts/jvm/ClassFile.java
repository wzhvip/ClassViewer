package com.nts.jvm;

import com.nts.jvm.attributeinfo.AttributeInfo;

import com.nts.jvm.constantpool.CP_info;
import com.nts.jvm.fieldinfo.FieldInfo;
import com.nts.jvm.methodinfo.MethodInfo;
import com.nts.jvm.util.ByteUtils;
import com.nts.jvm.util.ConstantPoolUtil;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;

public class ClassFile {

	// class文件
	// 类型 名称 数量
	// u4 magic 1
	// u2 minor_version 1
	// u2 major_version 1
	// u2 constant_pool_count 1
	// cp_info constant_pool constant_pool_count - 1
	// u2 access_flags 1
	// u2 this_class 1
	// u2 super_class 1
	// u2 interfaces_count 1
	// u2 interfaces interfaces_count
	// u2 fields_count 1
	// field_info fields fields_count
	// u2 methods_count 1
	// method_info methods methods_count
	// u2 attribute_count 1
	// attribute_info attributes attributes_count

	private int magic;

	private int minorVersion;

	private int majorVersion;

	public int getMagic() {
		return magic;
	}

	public void setMagic(int magic) {
		this.magic = magic;
	}

	public int getMinorVersion() {
		return minorVersion;
	}

	public void setMinorVersion(int minorVersion) {
		this.minorVersion = minorVersion;
	}

	public int getMajorVersion() {
		return majorVersion;
	}

	public void setMajorVersion(int majorVersion) {
		this.majorVersion = majorVersion;
	}

	public int getConstantPoolCount() {
		return constantPoolCount;
	}

	public void setConstantPoolCount(int constantPoolCount) {
		this.constantPoolCount = constantPoolCount;
	}

	public HashMap<Integer, CP_info> getCpInfo() {
		return cpInfo;
	}

	public void setCpInfo(HashMap<Integer, CP_info> cpInfo) {
		this.cpInfo = cpInfo;
	}

	public int getAccessFlags() {
		return accessFlags;
	}

	public void setAccessFlags(int accessFlags) {
		this.accessFlags = accessFlags;
	}

	public int getThisClass() {
		return thisClass;
	}

	public void setThisClass(int thisClass) {
		this.thisClass = thisClass;
	}

	public int getSuperClass() {
		return superClass;
	}

	public void setSuperClass(int superClass) {
		this.superClass = superClass;
	}

	public int getInterfacesCount() {
		return interfacesCount;
	}

	public void setInterfacesCount(int interfacesCount) {
		this.interfacesCount = interfacesCount;
	}

	public int getFieldsCount() {
		return fieldsCount;
	}

	public void setFieldsCount(int fieldsCount) {
		this.fieldsCount = fieldsCount;
	}

	public ArrayList<FieldInfo> getFields() {
		return fields;
	}

	public void setFields(ArrayList<FieldInfo> fields) {
		this.fields = fields;
	}

	public int getMethodsCount() {
		return methodsCount;
	}

	public void setMethodsCount(int methodsCount) {
		this.methodsCount = methodsCount;
	}

	public ArrayList<MethodInfo> getMethods() {
		return methods;
	}

	public void setMethods(ArrayList<MethodInfo> methods) {
		this.methods = methods;
	}

	public int getAttributeCount() {
		return attributeCount;
	}

	public void setAttributeCount(int attributeCount) {
		this.attributeCount = attributeCount;
	}

	public ArrayList<AttributeInfo> getAttributes() {
		return attributes;
	}

	public void setAttributes(ArrayList<AttributeInfo> attributes) {
		this.attributes = attributes;
	}

	public ArrayList<Integer> getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(ArrayList<Integer> interfaces) {
		this.interfaces = interfaces;
	}

	private int constantPoolCount;
	private HashMap<Integer, CP_info> cpInfo;
	private int accessFlags;
	private int thisClass;
	private int superClass;
	private int interfacesCount;
	private ArrayList<Integer> interfaces;
	private int fieldsCount;
	private ArrayList<FieldInfo> fields;
	private int methodsCount;
	private ArrayList<MethodInfo> methods;
	private int attributeCount;
	private ArrayList<AttributeInfo> attributes;

	@Override
	public String toString() {

		// class文件
		// 类型 名称 数量
		// u4 magic 1
		// u2 minor_version 1
		// u2 major_version 1
		// u2 constant_pool_count 1
		// cp_info constant_pool constant_pool_count - 1
		// u2 access_flags 1
		// u2 this_class 1
		// u2 super_class 1
		// u2 interfaces_count 1
		// u2 interfaces interfaces_count
		// u2 fields_count 1
		// field_info fields fields_count
		// u2 methods_count 1
		// method_info methods methods_count
		// u2 attribute_count 1
		// attribute_info attributes attributes_count

		StringBuffer sb = new StringBuffer();
		ByteBuffer magicBuff = ByteBuffer.allocate(4);
		magicBuff.putInt(magic);
		magicBuff.flip();
		String strMagic = "";
		for (int i = 0; i < 4; i++) {
			String c = ByteUtils.hex(magicBuff.get());
			strMagic += c;
		}
		strMagic = strMagic.replace("0x", "");

		sb.append("magic:" + strMagic + "\n");
		sb.append("minor_version:" + minorVersion + "\n");
		sb.append("major_version:" + majorVersion + "\n");
		sb.append("constant_pool_count:" + constantPoolCount + "\n");
		sb.append("     (\n");
		ConstantPoolUtil constantPoolUtil = new ConstantPoolUtil(cpInfo);
		for (int i = 1; i <= cpInfo.size(); i++) {
			if (cpInfo.get(i) != null)
				sb.append("     #" + i + "=" + constantPoolUtil.formatCpInfo(cpInfo.get(i)) + "\n");
		}

		sb.append("     )\n\n");

		sb.append("access_flags:" + accessFlags + "\n");
		sb.append("this_class:" + thisClass + "\n");
		sb.append("super_class:" + superClass + "\n");
		sb.append("interfaces_count:" + interfacesCount + "\n");
		sb.append("interfaces:\n{\n");
		for (int i = 0; i < interfaces.size(); i++) {
			sb.append("         " + interfaces.get(i) + "\n");
		}
		sb.append(" }\n");
		sb.append("fields_count:" + fieldsCount + "\n");
		sb.append("field_info:   \n");
		sb.append("     (\n");
		for (int i = 0; i < fields.size(); i++) {
			sb.append("         " + fields.get(i) + "\n");
		}
		sb.append("     )\n");
		sb.append("methods_count:" + methodsCount + "\n");
		sb.append("method_info: \n( \n");
		for (int i = 0; i < methods.size(); i++) {
			sb.append("     " + methods.get(i) + "\n");
		}
		sb.append(" )\n");
		sb.append("attribute_count:" + attributeCount + "\n");
		sb.append("attribute_info: \n(\n");
		for (int i = 0; i < attributes.size(); i++) {
			sb.append("         " + attributes.get(i) + "\n");
		}
		sb.append("     \n)\n");
		return sb.toString();

	}
}
